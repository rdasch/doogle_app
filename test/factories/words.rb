FactoryGirl.define do
  factory :word do |w|
    w.word { Faker::Lorem.word }
  end
  factory :valid_word, parent: :word do |w|
    w.word "reality"
  end
  factory :invalid_word, parent: :word do |w|
    w.word "kajsdhasjdkhg"
  end
end
