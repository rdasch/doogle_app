FactoryGirl.define do
  factory :definition do |d|
    d.definition { Faker::Lorem.sentence }
    association :word, factory: :valid_word
  end
end
