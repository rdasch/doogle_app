Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  get 'words/new'
  resources :words
  resources :definitions, only: [:create, :destroy]
  root 'words#new'
end
