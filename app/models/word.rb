class Word < ActiveRecord::Base
  has_many :definitions, dependent: :destroy
  before_save { word.downcase! }
  VALID_WORD_REGEX = /\A[a-zA-Z]+\z/
  validates :word, presence: true, format: { with: VALID_WORD_REGEX }
  validates_uniqueness_of :word, :case_sensitive => false
  
  def search_definitions
    search_dictionary_api
  end
  
  private
    def search_dictionary_api
      response = Dictionary.get_definitions(word)
      if response.kind_of?(Array)
        return response
      else
        return "Unknown error"
      end
    end
end
