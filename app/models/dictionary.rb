class Dictionary
  require 'rest-client'
  require 'nokogiri'
  
  def self.get_definitions(word)
    @base_url = "http://www.dictionaryapi.com/api/v1/references/collegiate/xml/"
    response = RestClient.get search_word_path(word)
    if response.code < 300
      return parse(response.body)
    else
      return response.code
    end
  end
  
  private
    def self.search_word_path(word)
      return "#{@base_url}#{word}?key=#{api_key}"
    end
    
    def self.api_key
      return "cab72891-f003-43ef-a983-253666d45082"
    end
    
    def self.parse(body)
      xml_doc = Nokogiri::XML(body)
      @definitions = Array.new
      xml_doc.xpath('//dt').each do |definition|
        @definitions << definition.text
      end
      return @definitions
    end
end