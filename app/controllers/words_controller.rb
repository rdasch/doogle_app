class WordsController < ApplicationController
  def show
    @word = Word.find_by(word_params[:word])
  end
  
  def new
    @word = Word.new
  end
  
  def create
    @error_message = nil
    @word = Word.find_by(word_params[:word])
    unless @word
      @word = Word.new(word_params[:word])
      @definitions = @word.search_definitions
      if @definitions != "Unknown error" && @word.valid? && @definitions.count > 0 && @word.save
        add_definitions
      elsif @word.valid? && @definitions.count > 0
        @error_message = "An error occurred on the server.  Please try again"
      end
    end
    render_definitions
  end
  
  private
    def word_params
      params.permit(:word => :word)
    end
  
    def add_definitions
      @definitions.each do |definition|
        @definition = @word.definitions.build(definition: definition)
        unless @definition.save
          return "Failure to save definitions to DB"
        end
      end
    end
    
    def render_definitions
      respond_to do |format|
        format.js { render 'create', layout: false }
      end
    end
end
