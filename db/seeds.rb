# Words
Word.create!(word: "Bigly")

# Definitions
words = Word.take(1)
1.times do
  definition = Faker::Lorem.sentence(1)
  words.each { |word| word.definitions.create!(definition: definition) }
end
