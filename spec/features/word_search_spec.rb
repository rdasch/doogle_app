require 'rails_helper'

feature 'Word search' do
  scenario 'with valid word', js: true do
    visit '/'
    expect(Word.count).to be 0
    expect(Definition.count).to be 0
    
    fill_in "word_word", with: "reality"
    click_button "Doogle"
    wait_for_ajax
    
    expect(page).to have_css("ul", :count => 1)
    expect(page).to_not have_content "No definitions found.  Try a different word."
    expect(Word.count).to be 1
    expect(Definition.count).to_not be 0
  end
  
  scenario 'with imaginary word', js: true do
    visit '/'
    expect(Word.count).to be 0
    expect(Definition.count).to be 0
    
    fill_in "word_word", with: "askdjhgsdjh"
    click_button "Doogle"
    
    expect(page).to_not have_css("ul")
    expect(page).to have_content "No definitions found.  Try a different word."
    expect(Word.count).to be 0
    expect(Definition.count).to be 0
  end
  
  scenario 'with invalid word', js: true do
    visit '/'
    expect(Word.count).to be 0
    expect(Definition.count).to be 0
    
    fill_in "word_word", with: "reality!"
    click_button "Doogle"
    
    expect(page).to_not have_css("ul")
    expect(page).to have_content "Word entered is invalid.  Please enter new word without any spaces or special characters."
    expect(Word.count).to be 0
    expect(Definition.count).to be 0
  end
end

def wait_for_ajax
  Timeout.timeout(30) do
    loop until finished_all_ajax_requests?
  end
end

def finished_all_ajax_requests?
  page.evaluate_script('jQuery.active').zero?
end