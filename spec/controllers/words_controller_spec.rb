require 'rails_helper'

describe WordsController, type: :controller do
  render_views
  describe "POST #create" do
    context "with valid word" do
      context "and word does not exist in DB" do
        it "creates word in DB" do
          expect{
            post :create, word: attributes_for(:valid_word), format: :js
          }.to change(Word,:count).by(1)
        end
        it "add definitions to DB" do
          expect {
            post :create, word: attributes_for(:valid_word), format: :js
          }.to change(Definition,:count).by_at_least(1)
        end
        it "renders definitions for word" do
          post :create, word: attributes_for(:valid_word), format: :js
          response.should render_template(:partial => '_definitions')
        end
      end
      context "and word exists in DB" do
        it "should not add word to DB" do
          create(:valid_word)
          expect{
            post :create, word: attributes_for(:valid_word), format: :js
          }.to_not change(Word,:count)
        end
        it "should render definitions" do
          create(:definition)
          post :create, word: attributes_for(:valid_word), format: :js
          response.should render_template(:partial => '_definitions')
        end
      end
    end
    context "with invalid word" do
      it "should not add word to DB" do
        expect{
          post :create, word: attributes_for(:invalid_word), format: :js
        }.to_not change(Word,:count)
        expect{
          post :create, word: attributes_for(:invalid_word, word: "reality1"), format: :js
        }.to_not change(Word,:count)
      end
    end
  end
end