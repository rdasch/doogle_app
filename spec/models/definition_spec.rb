require 'rails_helper'

describe Definition, type: :model do
  describe 'associations' do
    it { should belong_to(:word) }
  end
  
  describe 'validations' do
    it { should validate_presence_of :definition }
    it { should validate_presence_of :word_id }
  end
end