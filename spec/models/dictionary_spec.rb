require 'rails_helper'

describe Dictionary do
  describe ".get_definitions" do
    context "when word is a valid word" do
      before do
        @word = build_stubbed(:word, word: "reality")
      end
      it "returns an array of definitions" do
        expect(Dictionary.get_definitions(@word.word)).not_to be_empty 
      end
    end
    context "when word is invalid" do
      before do
        @word = build_stubbed(:word, word: "akjsdhaskjdh")
      end
      it "returns an empty array" do
        expect(Dictionary.get_definitions(@word.word)).to be_empty
      end
    end
  end
end