require 'rails_helper'

describe Word, type: :model do
  describe 'associations' do
    it { should have_many(:definitions).dependent(:destroy) }
  end
  
  describe 'validations' do
    it { should validate_presence_of :word }
    it { 
      create(:word)
      should validate_uniqueness_of(:word).case_insensitive
    }
    context "regular expression validations" do 
      it { should allow_value("word").for(:word) }
      it { should allow_value("WoRd").for(:word) }
      it { should_not allow_value("").for(:word) }
      it { should_not allow_value("   ").for(:word) }
      it { should_not allow_value("A sentence").for(:word) }
      it { should_not allow_value("Exclamation!").for(:word) }
      it { should_not allow_value("12345").for(:word) }
    end
  end
end